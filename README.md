# Framework DEV Underground

## O projeto

O **DEV Underground** é uma iniciativa de melhora do desenvolvimento das habilidades existentes e busca por aprendizado constante que essa profissão exige.
Dessa forma o framework vem como primeiro passo na produção conjuta de uma solução para o trabalho diário.


## O que é o framework?

Uma compilação do trabalho, experiências e da admiração pelo lema **DRY**.
Criamos um modo inicial de trabalho, como um boilerplate e unimos as ferramentas de SASS e GRUNTJS e o sistema Wordpress.
Esse é o primeiro passo, a contribuição está aberta e as melhorias são sempre bem vindas.

* Versão 0.0.1 - Modo inicial, testes e aplicação estão começando.


## Autores

* Allef Bruno [Link](https://www.link)
* Diego Curuma [Link's'](https://www.github.com/diegocuruma, https://www.behance.net/diegocuruma)
* Jonas Sousa [Behance](https://www.behance.net/onasousa)

## Referências 

* [Sass](http://sass-lang.com/)
* [Compass](http://compass-style.org/)
* [Grunt](http://gruntjs.com/)
* [Normalize CSS](http://necolas.github.io/normalize.css/)
* [HTML5 Boilerplate](http://html5boilerplate.com/)
* [HTML5 Shiv](https://github.com/aFarkas/html5shiv)
* [Clear Fix](http://nicolasgallagher.com/micro-clearfix-hack/)